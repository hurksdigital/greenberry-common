# `Models`

## Definition
In this folder all models are stored.

A model can be described with the following sub-types:
 - a core model
 - an entity model

...And can be implemented by:
 - front-ends, by mapping the API responses to the given interface
 - `TypeGraphQL` schemas, by implementing the properties with annotations to describe the behaviour for `TypeGraphQL`.
 - `ORM`, by implementing the properties with annotations to describe the behaviour for `TypeORM`. This can be used to generate database schema's.

#
## Core model
A core model is used to describe properties used in entity models.

#
## Entity model
Entity models are implementations for database entities, which can also be reused for schema implementations for GraphQL.