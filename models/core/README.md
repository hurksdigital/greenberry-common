# `Core models`

## Definition
In this folder all `Core Models` are stored.

A core model is an interface that describes a property of an entity model.

Because sequel languages have support for `JSON` nowadays, it is sometimes helpful not having to create an entire database entity to categorise some properties of the `Entity Model`.

For example, if you would have a property to store a 3D coordin ate, you could create the following type:

```
{
    x: number
    y: number
    z: number
}
```

Which could be a `core model`, though will be handled as `JSON` by `ORM`.

#
## Implementation
The core models will not be implemented in ORM / database, although they will be implemented by schemas in the `src/schemas` directory of projects extending this library for GraphQL support.

This means that if a `core model` does not belong to a `schema` it is not considered a core model and should be stored locally in the `extended project` instead of here.