/**
 * The default interface, which all entity models should extend.
 * 
 * @author Stan Hurks
 */
export default interface Entity {

    /**
     * The UUID of the entity.
     */
    uuid: string
}