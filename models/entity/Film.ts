import Entity from './Entity'

/**
 * The model for the `Film` entity.
 * 
 * @author Stan Hurks
 */
export default interface Film extends Entity {

    /**
     * The UUID of the entity.
     */
    uuid: string

    /**
     * The title of this film
     */
    title: string

    /**
     * The episode number of this film.
     */
    episode_id: number

    /**
     * The opening paragraphs at the beginning of this film.
     */
    opening_crawl: string

    /**
     * The name of the director of this film.
     */
    director: string

    /**
     * The name(s) of the producer(s) of this film.Comma separated.
     */
    producer: string

    /**
     * The date format of film release at original creator country.
     */
    release_date: Date

    /**
     * The time that this resource was created.
     */
    created: Date

    /**
     * The time that this resource was edited.
     */
    edited: Date
}