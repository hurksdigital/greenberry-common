# `Entity models`

## Definition
In this folder all `entity models` are stored.

A model represents an interface to be implemented by:
 - Front-ends
 - GraphQL schema's
 - ORM

#
## Implementation
A model can be an entity in the database and thus will always have a `uuid` property assigned to it and maybe other properties.

That is why every `Entity Model` should implement the `Entity` base class in order to remain consistent in the structure of your database.

If a model interface should not be stored in the database it should be placed in the `src/models/core` folder to become a `core model`.