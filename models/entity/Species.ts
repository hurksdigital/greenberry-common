import Entity from './Entity'
import Film from './Film'
import Planet from './Planet'

/**
 * The model for the `Species` entity.
 * 
 * @author Stan Hurks
 */
export default interface Species extends Entity {

    /**
     * The UUID of the entity.
     */
    uuid: string

    /**
     * The name of this species.
     */
    name: string

    /**
     * The classification of this species, such as "mammal" or "reptile".
     */
    classification: string

    /**
     * The designation of this species, such as "sentient".
     */
    designation: string

    /**
     * The average height of this species in centimeters.
     */
    average_height: string

    /**
     * The average lifespan of this species in years.
     */
    average_lifespan: string

    /**
     * A comma - separated string of common eye colors for this species, "none" if this species does not typically have eyes.
     */
    eye_colors: string

    /**
     * A comma - separated string of common hair colors for this species, "none" if this species does not typically have hair.
     */
    hair_colors: string

    /**
     * A comma - separated string of common skin colors for this species, "none" if this species does not typically have skin.
     */
    skin_colors: string

    /**
     * The language commonly spoken by this species.
     */
    language: string

    /**
     * The URL of a planet resource, a planet that this species originates from.
     */
    homeworld: Planet

    /**
     * An array of Film URL Resources that this species has appeared in.
     */
    films: Film[]

    /**
     * A date created from the ISO 8601 date format of the time that this resource was created.
     */
    created: Date

    /**
     * A date converted from the ISO 8601 date format of the time that this resource was edited.
     */
    edited: Date
}