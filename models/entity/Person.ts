import Film from './Film'
import Species from './Species'
import Starship from './Starship'
import Vehicle from './Vehicle'
import Entity from './Entity'
import Planet from './Planet'

/**
 * The model for the `Person` entity. Singular form of `People` is used here.
 * 
 * @author Stan Hurks
 */
export default interface Person extends Entity {

    /**
     * The UUID of the entity.
     */
    uuid: string

    /**
     * The name of this person.
     */
    name: string

    /**
     * The birth year of the person, using the in -universe standard of BBY or ABY - Before the Battle of Yavin or After the Battle of Yavin.The Battle of Yavin is a battle that occurs at the end of Star Wars episode IV: A New Hope.
     */
    birth_year: string

    /**
     * The eye color of this person.Will be "unknown" if not known or "n/a" if the person does not have an eye.
     */
    eye_color: string

    /**
     * The gender of this person.Either "Male", "Female" or "unknown", "n/a" if the person does not have a gender.
     */
    gender: string

    /**
     * The hair color of this person.Will be "unknown" if not known or "n/a" if the person does not have hair.
     */
    hair_color: string

    /**
     * The height of the person in centimeters.
     */
    height: string

    /**
     * The mass of the person in kilograms.
     */
    mass: string

    /**
     * The skin color of this person.
     */
    skin_color: string

    /**
     * The URL of a planet resource, a planet that this person was born on or inhabits.
     */
    homeworld: Planet

    /**
     * An array of films that this person has been in.
     */
    films: Film[]

    /**
     * An array of species that this person belongs to.
     */
    species: Species[]

    /**
     * An array of starships that this person has piloted.
     */
    starships: Starship[]

    /**
     * An array of vehicles that this person has piloted.
     */
    vehicles: Vehicle[]

    /**
     * the time that this resource was created.
     */
    created: Date

    /**
     * the time that this resource was edited.
     */
    edited: Date
}