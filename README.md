# `Greenberry Common`

## Definition
This project is used for sharing interfaces and functionality between all projects that belong to `Greenberry`.

#
## Keep it clean
Please keep this repository as clean as possible and do not just put everything here. Only store files in here that contain:
- Generic code
    - E.g.: Util classes to perform certain calculations.
- Interfaces that describe:
    - Entities in the database (`Entity Model`)
    - Properties of entities in the database (`Core Model`)

Do NOT store files in here that contain:
- Classified information
    - NEVER do this, as this library is available to the front-end.
- Non-generic code
    - Please keep this in the project that uses this code, as many projects may extend this library with many different people working on them. The files in this library should always remain clear to everyone.